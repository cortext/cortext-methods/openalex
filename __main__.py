#!/usr/bin/env python
# -*- coding: utf-8 -*-

#################################################
# Cortext Manager first calls this with python2 #
# and in that case we launch docker_wrapper     #
#################################################

from cortextlib.legacy import containerize  # noqa: F401

#################################
# Cortext Method Initialization #
#################################

from cortextlib.main import main

if __name__ == "__main__":
    main()
