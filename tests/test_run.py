import cortextlib
from cortextlib.data import Data
from cortextlib.database import Database
import openalex as cortext_openalex


def test_run(tmp_path):
    base_name = "test_name"
    cortext_openalex.Method(
        cortextlib.job.Job(
            {
                "script_path": "",
                "result_path": str(tmp_path),
                "corpus_file": "",
                "corpus_name": "test_corpus",
                "acquisition_procedure": "New corpus from a query",
                "limit": "10",
                "query_url": "https://api.openalex.org/works?"
                "page=1&filter=default.search:coriander+OR+cilantro",
                "base_name": base_name,
            }
        )
    ).run()
    data = Data(Database(tmp_path / f"{base_name}.db"))
    df, schema = data.to_dataframe()
