# openalex tests

## test case 1

1. upload the zip CSV file from `test/samples/small.zip`
2. run the CSV parser with default options
   (do not the robust CSV, but the old CSV parser)
3. run OpenAlex script using the db created by the CSV parser
   - select the "bibliodl" option
4. expected to have a CSV output with title and DOI
5. expected to have the db updated with a new doi table
