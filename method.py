# openalex - Cortext Openalex Method

import time

import pandas as pd

import cortextlib
from cortextlib import CortextMethod
from bibliodl import openalex as dl_openalex
from bibliodbs import query_downloader, biblio_openalex as db_openalex


class OpenalexMethod(CortextMethod):
    def __init__(self, job):
        super().__init__(job)
        self.method = self.params.pop("acquisition_procedure", None)

        # Param: limit
        max_records = 10000
        limit = self.params["limit"].strip()
        limit = int(limit) if limit else None
        assert limit is None or limit >= 0
        if limit is None or limit > max_records:
            limit = max_records
        if limit < 1:
            message = f"Invalid limit: {limit}. Pick from 1 to {max_records}."
            self.job.logger.warning(message)
            raise ValueError(message)
        self.params["limit"] = limit

        if "base_name" in self.params:
            base_name = self.params["base_name"].strip()
            if not len(base_name):
                message = f"Invalid base_name: {base_name}. Cannot be empty."
                self.job.logger.warning(message)
                raise ValueError(message)
        if "query_url" in self.params:
            query_url = self.params["query_url"].strip()
            if not len(query_url):
                message = f"Invalid query_url: {query_url}. Cannot be empty."
                self.job.logger.warning(message)
                raise ValueError(message)

    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)

    def main(self, **params):
        if self.method == "New corpus from a query":
            self.run_corpus_from_query(**params)
        elif self.method == "Enrich existing corpus":
            self.run_enrich_from_titles(**params)

    def run_enrich_from_titles(self, limit):
        """
        This function reads a sqlite database using cortextlib, search for a
        table name "title" containing titles of documents (such as papers,
        policies, reports, and so on), for each title it does an HTTP request to
        OpenAlex API using the bibliodl[1] to find a DOI number corresponding to
        the document title, the DOI number is recorded in a new table name "doi".
        An CSV file is saved in the job result_path with the title and doi for each
        document.

        ALERT! This function removes any "doi" table found on the sqlite database,
               and create an empty table to insert the DOI found on OpenAlex

        Output: {result_path}/openalex_records.csv

        - [1] https://gitlab.com/cortext/cortext-libraries/bibliodl
        """
        df = self.data.read_data_table("title")
        self.job.progress.set_total(df.size)
        if "doi" in self.data.table_names():
            self.data.drop_table("doi")
        cortextlib.data.create_table("doi", "text", self.data.db_con)
        for index, row in df.iterrows():
            title = row["data"]
            doi = ""
            res = dl_openalex.search(title)
            if res:
                doi = dl_openalex.find_doi(res)
                if not doi:
                    doi = ""
            cortextlib.data.insert_record(
                table_name="doi",
                source="Cortext OpenAlex title search",
                id=index[0],
                rank=index[1],
                parserank=index[2],
                data=doi,
                db_con=self.data.db_con,
            )
            self.job.progress.advance()
            time.sleep(0.1)
            if limit is not None:
                limit -= 1
                if limit == 0:
                    break
        self.data.db_con.commit()
        data, _ = self.data.to_dataframe(columns=["title", "doi"])
        data.to_csv(self.job.dir / "openalex_records.csv")

    def run_corpus_from_query(self, query_url, limit, base_name):
        """
        This function uses bibliodbs[1] to download records from OpenAlex API
        using a OpenAlex query url provided by the Cortext Manager user,
        and outputs raw records in JSON format as well as a CM database.

        Outputs:
        - {result_path}/{base_name}.jsonl.zst
        - {result_path}/{base_name}.db
        - {result_path}/{base_name}.yaml

        - [1] https://gitlab.com/cortext/cortext-libraries/bibliodbs
        """
        # Download from OpenAlex
        outpath = self.job.dir / f"{base_name}.jsonl.zst"
        oaqd = db_openalex.QueryDownloader(
            email_address="cortext@cortext.net",
            query_url=query_url,
            outpath=outpath,
        )
        oaqd.download(last=limit)

        # Extract and enrich
        records = query_downloader.iter_records(outpath)
        extracted = db_openalex.extract_records(records)
        enriched = db_openalex.RecordEnricher(
            email_address="cortext@cortext.net"
        ).enrich_records(extracted)
        df = pd.DataFrame(enriched)

        # Workaround for occasional 'first of first-cursor' duplicate from OpenAlex
        df = df.drop_duplicates("id")

        df["ISICitedRef"] = df["referenced_works_textual"].map(
            lambda rl: [None if x is None else x.replace(", ", "_") for x in rl]
        )
        df["ISIpubdate"] = df["publication_year"]

        # Store as Cortext DB
        df_schema = cortextlib.data.get_data_schema(df)
        db_path = self.job.dir / f"{base_name}.db"
        database = cortextlib.database.Database(db_path, base_name)
        data = cortextlib.data.Data(database, self.job.logger)
        data.from_dataframe(df, df_schema, data_source="openalex")
        cortextlib.database.register_new_database(data, corpus_type="openalex")
