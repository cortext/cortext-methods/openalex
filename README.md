# Cortext OpenAlex

Cortext script to collect bibliographic data using OpenAlex API.

It is implemented in Python using [cortextlib][], [bibliodbs][], and
[bibliodl][].

## Build docker image

```sh
docker build -t cortext-methods/openalex .
```

## License

Copyright (C) 2024 Cortext

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.

[cortextlib]: https://gitlab.com/cortext/cortext-methods/cortextlib
[bibliodbs]: https://gitlab.com/cortext/cortext-libraries/bibliodbs
[bibliodl]: https://gitlab.com/cortext/cortext-libraries/bibliodl
