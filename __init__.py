# openalex - Cortext Openalex Method

from .method import OpenalexMethod as Method

__all__ = ["Method"]
